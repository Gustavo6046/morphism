import threading
import cProfile
import inspect
import time
import trio
import wx
import collections


class wxTrioAppContext(object):
    pass

class wxTrioApp(object):
    app_type = wx.App

    def __init__(self):
        self.running = False
        self.wx_app = type(self).app_type()
        self._ctx = wxTrioAppContext()
        self.tps = 30
        self._semaphored = False
        self.thread_profile = None

    async def initialize(self, ctx):
        pass
    
    async def deinit(self, ctx):
        pass

    def blocks_semaphore(self, f):
        if inspect.iscoroutinefunction(f):
            async def _inner(*args, **kwargs):
                self._semaphored = True
                await f(*args, **kwargs)
                self._semaphored = False

        else:
            def _inner(*args, **kwargs):
                self._semaphored = True
                f(*args, **kwargs)
                self._semaphored = False
            
        return _inner

    def wx_loop(self):
        pr = cProfile.Profile()
        pr.enable()
        self.wx_app.MainLoop()

        if self.running:
            self.stop(False)
            
        pr.disable()

        self.thread_profile = pr

    def make_app_thread(self):
        assert not self.running, RuntimeError("This wxTrio app is already running!")

        t = threading.Thread(target=self.wx_loop, name="[wxTrio] wxPython thread")
        t.start()

        return t

    async def async_run(self, *args):
        await self.initialize(self._ctx, *args)
        self.make_app_thread()

        self.running = True
        await self._async_run(self._ctx)
        
        await self.deinit(self._ctx)

    def run_forever(self, *args):
        trio.run(self.async_run)

    async def _async_run(self, ctx):
        dtime = 0
        last_time = time.time()

        while self.running:
            await self.tick(ctx, dtime)

            while True:
                await trio.sleep(1 / self.tps)

                if not self._semaphored:
                    break

            new_time = time.time()
            dtime = new_time - last_time
            last_time = new_time

    async def tick(self, ctx, dtime):
        pass

    def stop(self, close = True):
        assert self.running, RuntimeError("This wxTrio app is not running already!")

        if close:
            self.wx_app.GetTopWindow().Close()

        self.wx_app.ExitMainLoop()
        self.running = False