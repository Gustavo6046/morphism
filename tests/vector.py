import collections
import math


COMPONENTS = {
    'x': 0,
    'y': 1,
    'z': 2,
    'w': 3,
}

class Rotation(object):
    def __init__(self, pitch = 0, yaw = 0, roll = 0):
        self.pitch = pitch
        self.yaw = yaw
        self.roll = roll

    def __iter__(self):
        return iter((self.pitch, self.yaw, self.roll))

def repr_frac(f):
    return int(f * 1000) / 1000

class CartesianVector(object):
    """A vector in Cartesian space.

        Defined as a 3D vector (which
    may represent a 2D vector if z is
    set to 0), with an optional homo-
    geneous coordinate value w.

        The actual   x, y and z values
    are divided by w. To retrieve them
    use the gx, gy and gz methods.
    """


    def __init__(self, x = 0, y = 0, z = 0, w = 1):
        """
        Arguments:
            x {float} -- The X coordinate.
            y {float} -- The Y coordinate.
        
        Keyword Arguments:
            z {float} -- The Z coordinate. Setting to 0 is equivalent to having a 2D vector. (default: {0})
            w {float} -- The homogeneous coordinate. (default: {1})
        """

        self.x = float(x)
        self.y = float(y)
        self.z = float(z)
        self.w = float(w)

    def gx(self):
        """Returns the effective Y coordinate of this vector,
        with homogeneous transforms applied.
        
        Returns:
            float -- The coordinate.
        """

        return self.homogeneous().x

    def gy(self):
        """Returns the effective Y coordinate of this vector,
        with homogeneous transforms applied.
        
        Returns:
            float -- The coordinate.
        """
        return self.homogeneous().y

    def gz(self):
        """Returns the effective Z coordinate of this vector,
        with homogeneous transforms applied.
        
        Returns:
            float -- The coordinate.
        """
        return self.homogeneous().z

    def __hash__(self):
        return hash(tuple(float(co) for co in (self.x, self.y, self.z, self.w)))

    def __iter__(self):
        return iter((self.x, self.y, self.z, self.w))

    def __getitem__(self, k):
        return (self.x, self.y, self.z, self.w)[COMPONENTS[k]]

    def set_coords(self, x = 0, y = 0, z = 0, w = 1):
        self.x = x
        self.y = y
        self.z = z
        self.w = w

    def __setitem__(self, k, v):
        k = k.lower()

        if k == 'x':
            self.x = v

        elif k == 'y':
            self.y = v
        
        elif k == 'z':
            self.z = v
        
        elif k == 'w':
            self.w = v

        else:
            raise IndexError('No such coordinate component: ' + k)

        #new_coords = [self.x, self.y, self.z, self.w]
        #new_coords[COMPONENTS[k]] = v
        #self.set_coords(*new_coords)

    def eff(self):
        """Constructs and returns a vector with
        the homogeneous coordinate reset to 1,
        as a convenient shorthand to the homogeneous
        method itself.
        
        Returns:
            CartesianVector -- A vector with w set to 1, and corresponding transformations applied.
        """
        return self.homogeneous()

    def homogeneous(self, w = 1):
        if self.w == w:
            return self

        """Constructs and returns a vector with the homogeneous
        coordinate applied and reset to the given parameter, if
        any.

        Arguments:
            w {float} --         The new homogeneous coordinate
                            value.
        
        Returns:
            CartesianVector   -- A new vector, with the changes
                            described above.
        """

        return CartesianVector(self.x * w / self.w, self.y * w / self.w, self.z * w / self.w, w)

    def __add__(self, v, w = 1.):
        """Adds two vectors, accounting for homogeneous space
        if necessary.
        
        Arguments:
            v {CartesianVector} -   The other vector in the summation.
        
        Keyword Arguments:   
            w {float} --            The new homogeneous coordinate. (default: {1.0})
        
        Returns:
            CartesianVector --      The summed vector.
        """

        #a = self.homogeneous(w)
        #b = v.homogeneous(w)

        ra = w / self.w
        rb = w / v.w

        return CartesianVector(
            self.x * ra + v.x * rb,
            self.y * ra + v.y * rb,
            self.z * ra + v.z * rb,
            w
        )

    def __neg__(self):
        return CartesianVector(-self.x, -self.y, -self.z, self.w)

    def __sub__(self, v, w = 1.):
        """Subtracts two vectors, accounting for homogeneous space
        if necessary.
        
        Arguments:
            v {CartesianVector} --  The other vector in the subtraction.
        
        Keyword Arguments:
            w {float} --            The new homogeneous coordinate. (default: {1.0})
        
        Returns:
            CartesianVector --      The subtracted vector.
        """
        return self + -v

    def __mul__(self, num, w = 1.0):
        """Multiplies two vectorial or scalar values, accounting for
        homogeneous space if necessary.
        
        Arguments:
            v {typing.Union[float, CartesianVector]} -  The multiplier.
        
        Keyword Arguments:   
            w {float} --                                The new homogeneous coordinate. (default: {1.0})
        
        Returns:
            CartesianVector --                          The new multiplied vector.
        """
        a = self.homogeneous(w)

        if isinstance(num, CartesianVector):
            b = num.homogeneous(w)

            return CartesianVector(a.x * b.x, a.y * b.y, a.z * b.z, w)

        else:
            return CartesianVector(a.x * num, a.y * num, a.z * num, w)

    def __div__(self, num, w = 1.0):
        """Divides two vectorial or scalar values, accounting for
        homogeneous space if necessary.
        
        Arguments:
            v {typing.Union[float, CartesianVector]} -  The divider.
        
        Keyword Arguments:   
            w {float} --                                The new homogeneous coordinate. (default: {1.0})
        
        Returns:
            CartesianVector --                          The new divided vector.
        """
        a = self.homogeneous(w)

        if isinstance(num, CartesianVector):
            b = num.homogeneous(w)

            return CartesianVector(a.x / b.x, a.y / b.y, a.z / b.z, w)

        else:
            return CartesianVector(a.x / num, a.y / num, a.z / num, w)

    def map(self, func):
        """Decorator to make generic coordinate
        mapping functions.
        
        Arguments:
            func {typing.Callable} -- The decorated function.

        Returns:
            function --     The transformed function. It takes as argument
                        a string (either of 'x', 'y' or 'z')   and a float
                        (the value of the coordinate to be transformed).
        """
        def __inner__(w = 1.0):
            """Returns the coordinates of the decorating
            vector, transformed by the decorated function.
            
            Keyword Arguments:
                w {float} --        An optional new homogeneous coordinate to be
                                set in the new vector.
                                (default: {1.0})
            
            Returns:
                CartesianVector -- The transformed vector.
            """

            a = self.homogeneous(w)

            return CartesianVector(func('x', a.x), func('y', a.y), func('z', a.z), w)

        return __inner__

    def __len__(self):
        """Returns the length of this vector."""

        v = self.eff()
        return math.sqrt(pow(v.x, 2) + pow(v.y, 2) + pow(v.z, 2))

    def sq_len(self):
        """Returns the square length of this vector, which is faster, since it
        ignores the square root stage."""

        v = self.eff()
        return pow(v.x, 2) + pow(v.y, 2) + pow(v.z, 2)

    def __pow__(self, exp = 1.0, w = 1.0):
        """Raises this vector to an exponent.
        
        Keyword Arguments:
            exp {float} -- The exponent value. (default: {1.0})
            w {float} -- An optional new homogeneous coordinate. (default: {1.0})
        
        Returns:
            CartesianVector -- The vector raised to the given exponent's power.
        """

        @self.map
        def power(_, n):
            return pow(n, exp)

        return power(w)

    def dot(self, vec):
        """Retrieves the dot product of this and another vector.
        
        Arguments:
            vec {CartesianVector} -- Another vector.
        
        Returns:
            [type] -- [description]
        """

        a = self.homogeneous()
        b = vec.homogeneous()

        return a.x * b.x + a.y * b.y + a.z * b.z

    def w_scale(self, scale = 1.0):
        """Scales the vector by changing its homogenoeus
        coordinate value.
        
        Keyword Arguments:
            scale {float} -- The scale. (default: {1.0})
        
        Returns:
            CartesianVector -- The new vector.
        """
        
        self.w /= scale
        return self

    def cross(self, vec, w = 1.0):
        """Returns the cross product of this vector
        and the supplied vector, optionally setting
        its homogeneous coordinate,   which is also
        taken into account.
        
        Arguments:
            vec {CartesianVector} -- The right-hand operand of the cross product.
        
        Keyword Arguments:
            w {float} -- The cross product's homogeneous coordinate position. (default: {1.0})
        
        Returns:
            CartesianVector -- The cross product.
        """

        a = self.homogeneous(w)
        b = vec.homogeneous(w)

        return CartesianVector(
            a.y * b.z - a.z * b.y,
            a.z * b.x - a.x * b.z,
            a.x * b.y - a.y * b.x,
            w
        )

    def __repr__(self):
        eff = self.homogeneous()
        return '{}(x={}, y={}, z={}, w={})'.format(type(self).__name__, repr_frac(self.x), repr_frac(self.y), repr_frac(self.z), repr_frac(self.w))

    def rodrigues(self, axis, rot):
        return self * math.cos(rot) + axis.cross(self) * math.sin(rot) + axis * axis.dot(self) * (1 - math.cos(rot))

    def x_rot(self, rot):
        #return CartesianVector(
        #    self.x,
        #    self.y * math.cos(rot) + self.z * -math.sin(rot),
        #    self.y * math.sin(rot) + self.z * math.cos(rot),
        #    self.w
        #)

        if rot == 0: return self

        a = self.y
        self.y = self.y * math.cos(rot) + self.z * -math.sin(rot)
        self.z = a * math.sin(rot) + self.z * math.cos(rot)

        return self

    def y_rot(self, rot):
        #return CartesianVector(
        #    self.x * math.cos(rot) + self.z * -math.sin(rot),
        #    self.y,
        #    self.x * math.sin(rot) + self.z * math.cos(rot),
        #    self.w
        #)

        if rot == 0: return self

        a = self.x
        self.x = self.x * math.cos(rot) + self.z * -math.sin(rot)
        self.z = a * math.sin(rot) + self.z * math.cos(rot)

        return self

    def z_rot(self, rot):
        # return CartesianVector(
        #     self.x * math.cos(rot) + self.y * -math.sin(rot),
        #     self.x * math.sin(rot) + self.y * math.cos(rot),
        #     self.z,
        #     self.w
        # )

        if rot == 0: return self

        a = self.x
        self.x = self.x * math.cos(rot) + self.y * -math.sin(rot)
        self.y = a * math.sin(rot) + self.y * math.cos(rot)

        return self

    def duplicate(self):
        return CartesianVector(self.x, self.y, self.z, self.w)

    def swap(self, coords, powers = (1, 1)):
        a = self[coords[1]]
        self[coords[1]] = self[coords[0]] * powers[0]
        self[coords[0]] = a * powers[1]

        return self

    def mul_coord(self, coord, power):
        self[coord] = self[coord] * power
        return self

    def rotate(self, ypw):
        return (self.duplicate()
            .z_rot(-ypw.yaw)
            .y_rot(ypw.pitch)
            .x_rot(ypw.roll)
        )


# Constant Vectors
CartesianVector.I = CartesianVector(1, 0, 0)
CartesianVector.J = CartesianVector(0, 1, 0)
CartesianVector.K = CartesianVector(0, 0, 1)