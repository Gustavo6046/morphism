import wx
import palette
import shademap
import hashlib
import random
import trio

from wx import xrc
from wxtrioapp import wxTrioApp
from ltbbase import MLTBaseFrame
from bufferedcanvas import *
from PIL import Image


class Identified(object):
    def __init__(self, *args, **kwargs):
        self._id = bytearray(tuple(random.randint(0, 255) for _ in range(30)))

        sha = hashlib.sha512()
        sha.update(bytes(self._id))
        self.id = sha.digest()

        self.__id_init__(*args, **kwargs)

    def __id_init__(self):
        pass

    def __hash__(self):
        return hash(self.id)

class MorphismLight(Identified):
    def __id_init__(self, owner, x, y, intensity = 2):
        self.owner = owner
        self.x = x
        self.y = y
        self.intensity = intensity

    def remove(self):
        self.owner.lights.remove(self)

class MorphismContext(object):
    def __init__(self):
        self.lights = set()

    def add_light(self, x, y, intensity = 2):
        MorphismLight(self, x, y, intensity)

class wxMorphismContext(bufferedcanvas.BufferedCanvas, MorphismContext):
    def __init__(self, *args, **kwargs):
        super(bufferedcanvas.BufferedCanvas, self).__init__(*args, **kwargs)
        super(MorphismContext, self).__init__()

    def draw(self, dc):
        # == WIṔ ==
        pass

class MorphismLTFrame(MLTBaseFrame):
    def __init__(self):
        super().__init__(None)

        self.canvas = wxMorphismContext()
        self.bSizer1.Add(self.canvas, 1, wx.EXPAND | wx.ALL, 5)

        self.SetSizer(bSizer1)
        self.Layout()

        self.Centre(wx.BOTH)
        
        self.Bind(wx.EVT_CLOSE, self.onClose)

    def onClose(self,event):
        self.Show(False)
        self.Destroy()

class wxMorphismLTApp(wx.App):
    pass

class MorphismLightingTest(wxTrioApp):
    async def initialize(self, ctx):
        pass