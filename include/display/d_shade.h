#ifndef HEADER_D_SHADE
#define HEADER_D_SHADE

#include "display/d_common.h"


unsigned char reshadeColor(g_shademap_t* shadeMap, unsigned char color, signed char shadeOffset);


#endif
