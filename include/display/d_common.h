#ifndef HEADER_D_COMMON
#define HEADER_D_COMMON


typedef struct g_color_s {
    unsigned char r;
    unsigned char g;
    unsigned char b;
} g_color_t;

typedef struct g_shadeentry_s {
    unsigned char dark;
    unsigned char bright;
} g_shadeentry_t;

typedef struct g_shademap_s {
    g_shadeentry_t* entries;
    unsigned short size;
} g_shademap_t;

typedef struct g_palette_s {
    g_color_t* entries;
    unsigned short size;
} g_palette_t;


#endif
