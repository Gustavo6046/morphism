#ifndef HEADER_G_ENTITY
#define HEADER_G_ENTITY

#include "game/ml_qlearn.h"



// === TYPES ===


// Routines and Actions
typedef enum {
    RIT_NOOP = 0,
    RIT_ACTION,
    RIT_SUBCALL
} g_rouitemtype_t;

typedef struct {
    char id[64];
    char args[64][7];
} g_eactioncall_t;

typedef struct {
    g_rouitemtype_t type;
    union ricall_u {
        g_eactioncall_t action; // 46 + null character
        char routine[16]; // 15 + null character
    } call;
} g_eroutineitem_t;

typedef struct {
    char id[16];
    unsigned char numItems;
    g_eroutineitem_t items[64];
} g_routine_t;

// Entities
typedef enum {
    VT_NUMERIC,
    VT_STRING
} g_valuetype_t;

typedef union g_propertyvalue_u {
    long numeric[6];
    char string[24];
} g_propertyvalue_t;

typedef struct {
    char id[9];
    g_valuetype_t valueType;
    g_propertyvalue_t value;
} g_eproperty_t;

typedef struct g_entitytype_s {
    char id[16]; // 16 + null character
    char name[24]; // 23 + null character
    char description[92]; // 91 + null character
    unsigned short base; // for property and action inheritance (PAI)
    g_eproperty_t typeProperties[12];
    unsigned char numRoutines;
    g_routine_t routines[64];
} g_entitytype_t;

typedef struct {
    char id[16];
} g_entityptr_t;

typedef struct g_entityptrlist_s {
    g_entityptr_t ptr;
    struct g_entityptrlist_s* next;
} g_entityptrlist_t;

typedef struct g_entity_s {
    // General
    char id[16];
    char name[32];
    unsigned short health;
    unsigned short x, y;
    unsigned short tagA, tagB, tagC;
    char type[16];
    unsigned char numProperties;
    g_eproperty_t properties[24];

    // AI
    g_entityptr_t target;
    g_entityptr_t owner;
    g_entityptrlist_t children;
    ml_qtable_t decider;

    // Display
    char currAnim[32];
    unsigned short frame;
    short frameTicks;
} g_entity_t;

typedef struct g_entitylist_s {
    unsigned char occupied;
    g_entity_t* entity;
    struct g_entitylist_s* next;
} g_entitylist_t;

unsigned short NUM_TYPES = 0;
g_entitytype_t* allEntityTypes[8192];

g_entitylist_t allEntities;



// === FUNCTIONS ===


// Entity building
g_entitytype_t* g_newEntityType();
g_entity_t* g_newEntity();
g_eproperty_t* g_getProperty(g_entity_t* from, char* id);
void g_setStringProperty(g_entity_t* to, char* id, char* value);
void g_setNumericProperty(g_entity_t* to, char id[8], unsigned short values, long* value);

// Routines
g_routine_t* g_newRoutine(char* id);
void g_addToRoutine(g_routine_t* routine, g_eroutineitem_t item);

g_routine_t* g_getRoutine(g_entitytype_t* from, char* id);
void g_addRoutine(g_entitytype_t* to, g_routine_t routine);

void g_runRoutine(g_entity_t* self, g_routine_t* routine);
void g_runRoutineItem(g_entity_t* self, g_eroutineitem_t* item);
void g_pickRoutine(g_entity_t* self, char* id);

// Q-Learning info
unsigned short g_getQState(g_entity_t* self);

// Display
void g_entityPlayAnim(g_entity_t* self, char* anim, unsigned short tickDuration);

// List management
void g_appendEntity(g_entity_t* entity, g_entitylist_t* list);
char g_removeEntity(g_entity_t* entity, g_entitylist_t* list);
g_entity_t* g_findEntity(char* id, g_entitylist_t* list);
g_entitytype_t* g_findEntityType(char* id);
g_entity_t* g_resolveEPtr(g_entityptr_t* ptr);

// Deallocation
void g_deleteEntityType(g_entitytype_t* type);
void g_despawnEntity(g_entity_t* entity);
void g_despawnList(g_entitylist_t* list);

#endif
