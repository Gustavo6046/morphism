#ifndef HEADER_ML_QLEARN
#define HEADER_ML_QLEARN


typedef long ml_qscore_t;
typedef unsigned short ml_action_t;

typedef struct ml_qtable_s {
    ml_qscore_t* items;
    ml_action_t actions;
    unsigned short states;
} ml_qtable_t;


ml_qscore_t     ml_qGetScore        (ml_qtable_t* table, ml_action_t action, unsigned short state);
void            ml_qSetScore        (ml_qtable_t* table, ml_action_t action, unsigned short state, ml_qscore_t newScore);
ml_qscore_t     ml_qGetScoreWithReward       (ml_qtable_t* table, unsigned short learnRate, unsigned short discount, ml_action_t prevAction, unsigned short prevState, unsigned short state, ml_qscore_t reward);
ml_qscore_t     ml_qApplyReward     (ml_qtable_t* table, unsigned short learnRate, unsigned short discount, ml_action_t prevAction, unsigned short prevState, unsigned short state, ml_qscore_t reward);
ml_qscore_t*    ml_qMaxScore        (ml_qtable_t* table, unsigned short state);
ml_qscore_t*    ml_qScorePointer    (ml_qtable_t* table, ml_action_t action, unsigned short state);
unsigned short  ml_qBestAction      (ml_qtable_t* table, unsigned short state);


#endif
