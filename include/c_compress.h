#ifndef HEADER_C_COMPRESS
#define HEADER_C_COMPRESS


// Simple binary data representation.
// Points to data in memory.
typedef struct {
    unsigned long length;
    unsigned char* data;
} c_binary_t;

// Huffman-like compressed data structure format:
// 1            4            8            9          9 + DICTLENG    9 + DICTLENG + DATALENG
// X NUMITEMS - X DATALENG - X DICTLENG - X (DICT) - X (DATA) - - - - X

// DICT: [char1] [char2] ... [charN]
//   where N = DICTLENG
// DATA: a NUMITEMS-long sequence of items, where each item
//       is a small sequence of bits:
//   0b0           - char1
//   0b10          - char2
//   ...
//   (1 << N) - 1  - charN

// Usually char1 is the most frequent item, while
// char2 is the least frequent one.

typedef struct {
    unsigned long numItems;
    unsigned long dataLength;
    unsigned char dictLength;
    unsigned char* dict;
    unsigned char* data;
} c_huffmanlike_t;

//c_binary_t* c_rle_encode(c_binary_t* data);
//c_binary_t* c_rle_decode(c_binary_t* compressed);

c_huffmanlike_t* c_hufflike_encode(c_binary_t* data);
c_binary_t* c_hufflike_decode(c_huffmanlike_t* compressed);

void c_binary_dealloc(c_binary_t* data);
void c_hufflike_dealloc(c_huffmanlike_t* data);


#endif
