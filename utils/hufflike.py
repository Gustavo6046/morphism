# // Huffman-like compressed data structure format:
# // 1            4            8            10         11 + DICTLENG    11 + DICTLENG + DATALENG
# // X NUMITEMS - X DATALENG - X DICTLENG - X (DICT) - X (DATA) - - - - X
#
# // DICT: [char1] [char2] ... [charN]
# //   where N = DICTLENG
# // DATA: a NUMITEMS-long sequence of items, where each item
# //       is a small sequence of bits:
# //   0b0           - char1
# //   0b10          - char2
# //   ...
# //   (1 << N) - 1  - charN
#
# // Usually char1 is the most frequent item, while
# // char2 is the least frequent one.
#
# typedef struct {
#     unsigned long numItems;
#     unsigned long dataLength;
#     unsigned short dictLength;
#     unsigned char* dict;
#     unsigned char* data;
# } c_huffmanlike_t;

import struct


def hufflike_encode(data):
    num_items = len(data)
    
    header = bytearray(struct.pack('I', num_items))
    res_data = bytearray()

    hdict = []
    hdict_pos = [-1 for _ in range(256)]
    frequencies = [0 for _ in range(256)]
    dict_len = 0
    bit_pos = 0
    cur_byte = 0

    for c in data:
        if frequencies[c] == 0:
            dict_len += 1

        frequencies[c] += 1

    for fi in range(256):
        if frequencies[fi] > 0:
            done = False

            for i in range(len(hdict)):
                if frequencies[hdict[i]] < frequencies[fi]:
                    hdict.insert(i, fi)
                    done = True

                    break
                
            if not done:
                hdict.append(fi)

    for i in range(len(hdict)):
        hdict_pos[hdict[i]] = i

    for i in range(num_items):
        if hdict_pos[data[i]] == 0:
            bit_pos += 1

            if bit_pos >= 8:
                res_data += bytearray([cur_byte])
                cur_byte = 0
                bit_pos -= 8

        else:
            for ibit in range(hdict_pos[data[i]], -1, -1):
                if ibit: cur_byte |= 1 << (7 - bit_pos)

                bit_pos += 1

                if bit_pos >= 8:
                    res_data += bytearray([cur_byte])
                    cur_byte = 0
                    bit_pos -= 8

    if bit_pos > 0:
        res_data += bytearray((cur_byte,))

    return header + struct.pack('IB', len(res_data), len(hdict)) + bytearray(hdict) + res_data
    
def hufflike_decode(compressed):
    num_items, data_len, hdict_len = struct.unpack('IIB', compressed[: 9])

    hdict = compressed[9: 9 + hdict_len]
    data = compressed[9 + hdict_len: 9 + hdict_len + data_len]

    res = bytearray()

    cur_byte = 0
    cur_bit = 0
    cur_item = 0
    item_accumulator = 0

    while cur_item < num_items:
        if (data[cur_byte] >> (7 - cur_bit)) & 1:
            item_accumulator += 1

        else:
            res.append(hdict[item_accumulator])
            item_accumulator = 0
            cur_item += 1

        cur_bit += 1

        if cur_bit > 7:
            cur_bit = 0
            cur_byte += 1

    return res