import PIL.Image, PIL.ImageDraw
import sys
import re
import math
import utilbase
import palette


class PaletteRenderer(utilbase.Utility):
    name = 'PalerPal - The Palette Rendering Pal'
    description = [
        'Loads a binary Morphism Palette file (.mpl)',
        'and exports a PNG containing rows of the palette\'s',
        'colors.'
    ]

    def help_list(self):
        yield '- an input filename (extension is optional)'

    def main(self, in_fn, out_fn):
        pal = palette.Palette(in_fn)

        self.log('INFO', f'Read {len(pal)} colors from {in_fn}')

        cols = 16
        rows = math.ceil(len(pal) / 16)

        img = PIL.Image.new('RGBA', (16 * cols, 16 * rows), color = (0, 0, 0, 0))
        draw = PIL.ImageDraw.Draw(img)

        for i, col in pal:
            x = i % cols * 16
            y = math.floor(i / cols) * 16
            draw.rectangle((x, y, x + 15, y + 15), fill = tuple(col))

        self.log('INFO', f'Rendered palette image.')

        img.save(out_fn)
        self.log('INFO', f'Saved palette image. Showing...')
        img.show()
        
        self.log('INFO', f'Shown, closing. Bye!')
        return 0

    def run(self, argv):
        if len(argv) <= 1 or argv[1] in ('--help', '-h'):
            self.print_help()
            return int(len(sys.argv) > 1)

        else:
            arg = re.sub(r'\.mpl$', '', sys.argv[1])
            return self.main(arg + '.mpl', arg + '.png')

if __name__ == "__main__":
    PaletteRenderer(sys.argv)