import sys


def strhash(s):
    h = 5381

    for c in s:
        h = (((h << 5) + h) & ((1 << (4 * 8)) - 1)) ^ ord(c)

    return h

if __name__ == "__main__":
    print('0x' + hex(strhash(' '.join(sys.argv[1:])))[2:].upper())