#include <assert.h>
#include <conio.h>
#include <dos.h>
#include "display/d_common.h"
#include "dos/d_vga.h"


unsigned char VGA_ENABLED = 0;


void g_vga_setMode(unsigned char mode) {
    union REGS regs;

    regs.h.ah = 0;
    regs.h.al = mode;

    int386(0x10, &regs, &regs);
}

void g_vga_plotPixel(unsigned short x, unsigned short y, unsigned char color) {
    assert(x < 320);
    assert(y < 200);

    VGA[y * 320 + x] = color;
}

void g_vga_enable() {
    if (VGA_ENABLED)
        return;

    g_vga_setMode(VGA_MODE_200P_256C);
    VGA_ENABLED = 1;
}

void g_vga_disable() {
    if (!VGA_ENABLED)
        return;

    g_vga_setMode(VGA_MODE_TEXT);
    VGA_ENABLED = 0;
}

void g_vga_setPalette(g_palette_t* palette) {
    unsigned short i;

    outp(VGA_PALETTE_INDEX, 0);

    for (i = 0; i < palette->size; i++) {
        outp(VGA_PALETTE_DATA, palette->entries[i].r);
        outp(VGA_PALETTE_DATA, palette->entries[i].g);
        outp(VGA_PALETTE_DATA, palette->entries[i].b);
    }
}
