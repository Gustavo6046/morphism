// String hasher by Dan Bernstein @ Usenet comp.lang.c.
// All rights to c_hash.h are reserved to Dan.

const unsigned long strhash(char *str) {
    unsigned long hash = 5381;
    int c;

    while (c = *str++)
        hash = ((hash << 5) + hash) ^ c;

    return hash;
}

const unsigned long bytehash(unsigned long length, unsigned char *data) {
    unsigned long hash = 5381;

    while (length-- > 0)
        hash = ((hash << 5) + hash) ^ *(++data);

    return hash;
}