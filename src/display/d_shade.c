#include "display/d_common.h"
#include "display/d_shade.h"
#include <assert.h>


unsigned char reshadeColor(g_shademap_t* shadeMap, unsigned char color, signed char shadeOffset) {
    unsigned char res = color;
    unsigned char darken = shadeOffset < 0;

    assert(color < shadeMap->size);

    while (shadeOffset != 0) {
        if (darken) {
            res = shadeMap->entries[color].dark;
            shadeOffset++;
        }

        else {
            res = shadeMap->entries[color].bright;
            shadeOffset--;
        }

        assert(res < shadeMap->size);
    }

    return res;
}
