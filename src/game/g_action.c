#include "game/g_action.h"
#include "game/g_entity.h"
#include <stdlib.h>


void g_runAction(g_entity_t* self, char action[64], char args[64][7]) {
    char* dummyS;
    long nums[6];
    unsigned short i, j;

    switch (strhash(action)) {
        case ACTHASH_DESPAWN:
            g_despawnEntity(self);
            return;

        case ACTHASH_SETPROPERTYSTR:
            g_setStringProperty(self, args[0], args[1]);
            return;

        case ACTHASH_SETPROPERTYNUM:
            nums[0] = strtol(args[1], &dummyS, 10);
            g_setNumericProperty(self, args[0], 1, nums);
            return;

        case ACTHASH_SETPROPERTYNUMS:
            j = strtol(args[2], &dummyS, 10);

            for (i = 0; i < j; i++)
                nums[i] = strtol(args[i + 3], &dummyS, 10);

            g_setNumericProperty(self, args[0], 1, nums);
            return;

        case ACTHASH_PLAYANIM:
            g_entityPlayAnim(self, args[0], strtol(args[1], &dummyS, 10));
            return;
    }
}
