#include "c_log.h"
#include <stddef.h>
#include <stdio.h>


void c_openLogFile(char* logFn) {
    if (logFile != NULL)
        fclose(logFile);

    logFile = fopen(logFn, "w");
}

void c_logLine(char* kind, char* msg) {
    char* fmsg = sprintf("%-12s %s\n", sprintf("[%s]", kind), msg);

    printf(fmsg);
    fprintf(logFile, fmsg);
}